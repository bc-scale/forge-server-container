# eyegog's Minecraft forge server container
A minecraft forge server in a docker container.

## Usage

Pull the image:
```
docker pull eyegog/forge-server
```

Run the container with no mounts:
```
docker run --name "normal" eyegog/forge-server:latest
```

Run the container with mounted world directory and configuration, and expose port `25566`:
```
docker run --name "normal" \
	-v "/srv/forge/servers/normal/mods:/mods" \
	-v "/srv/forge/servers/normal/server.properties:/server/server.properties" \
	-v "/srv/spigot/servers/normal/world:/server/world" \
	-p "25566:25565" \
	eyegog/forge-server:latest
```

### Configuration

The server binaries and config are located at `/server` within the container. After the container has been run and the binary generates the inital configuration, `/server` will look like this:
```
/server
├── banned-ips.json
├── banned-players.json
├── config
├── defaultconfigs
├── eula.txt
├── forge-1.19.2-43.1.3-installer.jar
├── forge-1.19.2-43.1.3-installer.jar.log
├── libraries
├── logs
├── mods
├── ops.json
├── run.bat
├── run.sh
├── server.properties
├── usercache.json
├── user_jvm_args.txt
├── whitelist.json
└── world
```

**Using predefined confgiuration**
- You can [bind mount](https://docs.docker.com/storage/bind-mounts/) your own configuration and world files into this directory for use at runtime.

**Mods**
- Bind mount mod java files to `/server/mods`

#### Rcon
Minecraft's `rcon` ('remote console') protocol is a  _very_ simple protocol for executing server commands remotely. It is a rather useful feature of minecraft servers, but is entirely unencrypted.

In an effort to try and better the security of using rcon, the [mcrcon](https://github.com/Tiiffi/mcrcon) rcon client has also been installed within the container. This way rcon can be enabled on the server but can only be used from within the container via `127.0.0.1`/`localhost`. 

For example, to execute a server command from the container host:
```bash
docker exec -it <container_name> mcrcon -p <rcon_password> <command>
```

```bash
docker exec -it mc_normal mcrcon -p "MyRconPassword" "save-all"
```

Or even from a remote machine over ssh:
```bash
ssh minecraft.example.org -t 'docker exec -it mc_normal mcrcon -p "MyRconPassword" "save-all"'
```

_You may also ignore this method entirely and simpoly publish the rcon port from the container._
