FROM debian:bullseye

RUN apt update -y && apt install -y openjdk-17-jre openjdk-17-jdk wget git make gcc

WORKDIR /
RUN git clone https://github.com/Tiiffi/mcrcon.git
WORKDIR /mcrcon
RUN make && make install

RUN mkdir /server
WORKDIR /server

RUN wget https://maven.minecraftforge.net/net/minecraftforge/forge/1.19.2-43.1.57/forge-1.19.2-43.1.57-installer.jar
RUN java -jar forge-1.19.2-43.1.57-installer.jar --installServer

RUN echo "eula=true" > eula.txt

EXPOSE 25565

CMD /server/run.sh --nogui
